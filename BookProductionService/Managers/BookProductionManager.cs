using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookProductionService.Models.Record.Family;
using BookProductionService.Models.Record.Individual;

public class BookProductionManager : IBookProductionManager
{
    private IRecordRepository __RecordRepository;
    private IUtilitiesManager __UtilitiesManager;

    public BookProductionManager(IRecordRepository recordRepsitory, IUtilitiesManager utilitiesManager)
    {
        __RecordRepository = recordRepsitory;
        __UtilitiesManager = utilitiesManager;
    }

    public List<Record> GetRecords()
    {
        return __RecordRepository.Get();
    }

    public string GetHTMLStringForCustomBook(string id, string familyID)
    {
        return GetHTMLString(id, familyID, true);
    }

    public string GetHTMLStringForCompleteBook(string id)
    {
        return GetHTMLString(id, string.Empty, false);
    }

    private string GetHTMLString(string id, string familyID, bool isCustomBook)
    {
        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h1>Family History Book</h1></div>
                                <div class='content'>");

        if (isCustomBook)
        {
            _StringBuilder.Append(GetCustomRecordDetails(id, familyID));
        }
        else
        {
            _StringBuilder.Append(GetRecordDetails(id));
        }

        _StringBuilder.Append(@"
                                </div>
                            </body>
                        </html>");

        return _StringBuilder.ToString();
    }

    private string GetRecordDetails(string id)
    {
        Record _Record = GetRecords().Where(record => record.ID == id).SingleOrDefault();
        StringBuilder _RecordDetails = new StringBuilder();

        foreach (FamilyModel family in _Record.Family)
        {
            _RecordDetails.AppendLine(GetFamilyDetails(family, "1"));
        }

        return _RecordDetails.ToString();
    }

    private string GetCustomRecordDetails(string id, string familyID)
    {
        Record _Record = GetRecords().Where(record => record.ID == id).SingleOrDefault();
        StringBuilder _RecordDetails = new StringBuilder();

        if (_Record.Family[0].ID.ToString() == familyID)
        {
            _RecordDetails.AppendLine(GetFamilyDetails(_Record.Family[0], "1"));
        }
        else
        {
            _RecordDetails.Append(FindFamilyInTree(_Record.Family[0], familyID).ToString());
        }

        return _RecordDetails.ToString();
    }

    public StringBuilder FindFamilyInTree(FamilyModel family, string familyID)
    {
        StringBuilder _RecordDetails = null;

        foreach (FamilyModel childFamily in family.ChildrenFamily)
        {
            if (childFamily.ID.ToString() == familyID)
            {
                _RecordDetails = new StringBuilder();
                _RecordDetails.AppendLine(GetFamilyDetails(childFamily, "1")).ToString();
                return _RecordDetails;
            }
            else
            {
                _RecordDetails = FindFamilyInTree(childFamily, familyID);
                if (_RecordDetails != null)
                {
                    return _RecordDetails;
                }
            }
        }
        return _RecordDetails;
    }



    private string GetFamilyDetails(FamilyModel family, string level)
    {
        StringBuilder _FamilyDetails = new StringBuilder();

        _FamilyDetails.Append(level + ". ");

        if (family.Father != null)
        {
            _FamilyDetails.Append(GetDetails(family.Father));
        }

        if (family.Mother != null)
        {
            _FamilyDetails.Append(GetDetails(family.Mother));
        }

        if (family.ChildrenFamily.Count != 0)
        {
            _FamilyDetails.Append(GetChildrenDetails(family.ChildrenFamily));
        }

        int _ChildLevel = 0;
        foreach (FamilyModel childrenFamily in family.ChildrenFamily)
        {
            _ChildLevel++;
            _FamilyDetails.Append("<blockquote>");
            string _CurrentLevel = level + "." + _ChildLevel;
            _FamilyDetails.Append(GetFamilyDetails(childrenFamily, _CurrentLevel));
            _FamilyDetails.Append("</blockquote>");
        }

        return _FamilyDetails.ToString();
    }

    private string GetDetails(IndividualModel individual)
    {
        StringBuilder _IndividualDetails = new StringBuilder();
        _IndividualDetails.Append(__UtilitiesManager.GetName(individual.Name, individual.Birth.Date, individual.Birth.Place));

        if (individual.Death.Date != "N/A")
        {
            _IndividualDetails.Append(__UtilitiesManager.GetDeath(individual.Name, individual.Death.Date, individual.Death.Place));
        }
        return _IndividualDetails.ToString();
    }

    private string GetChildrenDetails(List<FamilyModel> children)
    {
        StringBuilder _FamilyDetails = new StringBuilder();
        _FamilyDetails.Append("They had the following children:");
        _FamilyDetails.Append("</br>");

        foreach (FamilyModel child in children)
        {
            string _Name = string.Empty;
            if (child.Father != null)
            {
                _Name = child.Father.IsAncestor ? child.Father.Name : child.Mother.Name;
            }
            else
            {
                _Name = child.Mother.Name;
            }

            _FamilyDetails.Append("- ");
            _FamilyDetails.Append(_Name);
            _FamilyDetails.Append("</br>");

        }
        return _FamilyDetails.ToString();
    }

    public List<FamilyModel> GetFamilies(string id)
    {
        List<Record> _Trees = __RecordRepository.Get();
        Record _Tree = _Trees.Where(tree => tree.ID.ToString() == id).SingleOrDefault();

        List<FamilyModel> _Families = new List<FamilyModel>()
        {
            _Tree.Family.Single()
        };

        GetFamily(_Families, _Tree.Family.Single());
        return _Families;
    }

    private void GetFamily(List<FamilyModel> allFamilies, FamilyModel parentFamily)
    {
        foreach (FamilyModel family in parentFamily.ChildrenFamily)
        {
            allFamilies.Add(family);
            GetFamily(allFamilies, family);
        }
    }
}