public interface IUtilitiesManager
{
    string GetName(string name, string DOB, string location);
    string GetDeath(string name ,string date, string location);
    string GetAddress(string name, string date, string location);
    string GetChristening(string name, string date, string location);
}