using System.Collections.Generic;
using BookProductionService.Models.Record.Family;

public interface IBookProductionManager
{
    List<Record> GetRecords();
    string GetHTMLStringForCompleteBook(string id);
    string GetHTMLStringForCustomBook(string treeID, string familyID);
    List<FamilyModel> GetFamilies(string id);
}