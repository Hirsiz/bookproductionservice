using System.Collections.Generic;
using System.IO;

public class UtilitiesManager : IUtilitiesManager
{
    private Dictionary<string, string> __Phrases;

    public UtilitiesManager()
    {
        __Phrases = new Dictionary<string, string>();
        ParseCSVFile();

    }

    private void ParseCSVFile()
    {
        using (var reader = new StreamReader(@"Assets/Phrases.txt"))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');

                __Phrases.Add(values[0], values[1]);
            }
        }
    }

    public string GetName(string name, string DOB, string location)
    {
        string _Pharse = __Phrases["basic"];
        return string.Format(_Pharse, name, DOB, location);
    }

    public string GetDeath(string name, string date, string location)
    {
        string _Pharse = __Phrases["death"];
        return string.Format(_Pharse, name, date, location);
    }

    public string GetAddress(string name, string date, string location)
    {
        string _Pharse = __Phrases["address"];
        return string.Format(_Pharse, name, date, location);
    }

    public string GetChristening(string name, string date, string location)
    {
        string _Pharse = __Phrases["christening"];
        return string.Format(_Pharse, name, date, location);
    }
}