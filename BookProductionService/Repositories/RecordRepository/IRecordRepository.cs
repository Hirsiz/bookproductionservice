using System.Collections.Generic;

public interface IRecordRepository
{
    List<Record> Get();
    Record Get(string id);
    Record Create(Record record);
    void Update(string id, Record recordIn);
    void Remove(Record recordIn);
    void Remove(string id);
    void RemoveMany(string fileID);
}