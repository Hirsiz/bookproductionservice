﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Mvc;
using IronPdf;
using BookProductionService.Models.Record.Family;

namespace BookProductionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookProductionController : ControllerBase
    {
        private IBookProductionManager __BookProductionManager;
        private IConverter __Converter;

        public BookProductionController(IBookProductionManager bookProductionManager, IConverter converter)
        {
            __BookProductionManager = bookProductionManager;
            __Converter = converter;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<List<Record>> Get()
        {
            return __BookProductionManager.GetRecords();
        }

        // GET api/values/5
        [HttpGet]
        [Route("download/{id}")]
        public ActionResult<FileContentResult> GetBook(string id)
        {
            // var PDF = HtmlToPdf.StaticRenderUrlAsPdf(new Uri("https://en.wikipedia.org"));
            // return File(PDF.BinaryData, "application/pdf", "Wiki.Pdf");
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "Family History Book Report"
            };

            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = __BookProductionManager.GetHTMLStringForCompleteBook(id),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Produced by MyStory" }
            };

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            var file = __Converter.Convert(pdf);
            return File(file, "application/pdf", "Family History Book.pdf");
        }

        [HttpGet]
        [Route("DownloadCustom")]
        public ActionResult<FileContentResult> GetCustomBook(string treeID, string familyID)
        {
            // var PDF = HtmlToPdf.StaticRenderUrlAsPdf(new Uri("https://en.wikipedia.org"));
            // return File(PDF.BinaryData, "application/pdf", "Wiki.Pdf");
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report"
            };

            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = __BookProductionManager.GetHTMLStringForCustomBook(treeID, familyID),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" }
            };

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            var file = __Converter.Convert(pdf);
            return File(file, "application/pdf", "Family History Book.pdf");
        }

        [HttpGet]
        [Route("getFamilies")]
        public ActionResult<List<FamilyModel>> GetFamilies(string id)
        {
            return __BookProductionManager.GetFamilies(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
