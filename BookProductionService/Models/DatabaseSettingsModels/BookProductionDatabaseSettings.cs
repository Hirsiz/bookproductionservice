namespace BookProductionService.Models.DatabaseSettingsModels
{
    public class BookProductionDatabaseSettings : IBookProductionDatabaseSettings
    {
        public string BookProductionCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}