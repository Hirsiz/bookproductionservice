namespace BookProductionService.Models.DatabaseSettingsModels
{
    public interface IBookProductionDatabaseSettings
    {
        string BookProductionCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}