using MongoDB.Bson;

namespace BookProductionService.Models.Record.Individual
{
    public class IndividualModel
    {
        public ObjectId _id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public AddressModel Address { get; set; } = new AddressModel();
        public BirthModel Birth { get; set; } = new BirthModel();
        public DeathModel Death { get; set; } = new DeathModel();
        public bool IsAncestor { get; set; }
        //add fact object
    }
}