using System;

namespace BookProductionService.Models.Record.Individual
{
    public class BirthModel
    {
        public string Place { get; set; }
        public string Date { get; set; }
    }
}