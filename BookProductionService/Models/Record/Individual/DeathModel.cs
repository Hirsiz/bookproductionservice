using System;

namespace BookProductionService.Models.Record.Individual
{
    public class DeathModel
    {
        public string Place { get; set; } 
        public string Date { get; set; }
    }
}