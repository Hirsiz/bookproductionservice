using System;

namespace BookProductionService.Models.Record.Family
{
    public class Marriage
    {
        public string Place { get; set; } 
        public DateTime Date { get; set; }

    }
}