using System.Collections.Generic;
using BookProductionService.Models.Record.Individual;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BookProductionService.Models.Record.Family
{
    public class FamilyModel
    {
        [BsonId]
        [BsonElement("ID")]
        public ObjectId ID { get; set; }
        public Marriage Marriage { get; set; }
        public IndividualModel Father { get; set; }
        public IndividualModel Mother { get; set; }
        public List<FamilyModel> ChildrenFamily { get; set; } = new List<FamilyModel>();
        public string FamilyName { get; set; }
        public List<string> SubMaintainerIDs { get; set; } = new List<string>();
    }
}