﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookProductionService.Models.DatabaseSettingsModels;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BookProductionService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // requires using Microsoft.Extensions.Options
            services.Configure<BookProductionDatabaseSettings>(
                Configuration.GetSection(nameof(BookProductionDatabaseSettings)));

            //Managers
            services.AddScoped<IBookProductionManager, BookProductionManager>();
            services.AddScoped<IUtilitiesManager, UtilitiesManager>();

            //Repositories
            services.AddSingleton<IRecordRepository, RecordRepository>();

            services.AddSingleton<IBookProductionDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<BookProductionDatabaseSettings>>().Value);
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));

            //Cors options
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors("AllowAll");
            app.UseMvc();
        }
    }
}
